function getCourseList(resp)
{
    if (resp) {
        displayMessage(resp);
    }
    displayList('/api/course/getlist', 'course-hb-tmpl', 'course-list');
}

$(document).ready(function () {
    var formId = 'course-form';
    formHandler($('#'+formId), getCourseList, null, genericBeforeSend);
    getCourseList();
});