function formHandler(formElement, successCallback, errorCallback, beforeSendCallback, noData)
{
    $.validate({
        form: '#'+formElement.attr('id'),
        onSuccess: function(form) {
            var data = noData === true ? null : form.serialize();
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: data,
                dataType: 'json',
                beforeSend: function(xhr) {
                    if (beforeSendCallback) {
                        beforeSendCallback(xhr, form);
                    }
                },
                error: function(a, b, c) {
                    if (errorCallback) {
                        errorCallback(a,b,c,form);
                    }
                    else {
                        displayMessage({error: true, message: 'Error: '+b});
                    }
                },
                success: function(response) {
                    // reset form
                    form[0].reset();
                    
                    if (successCallback) {
                        successCallback(response, form);
                    }
                    else {
                        displayMessage(response);
                    }
                }
            });
            return false;
        }
    });
}

function genericBeforeSend(xhr)
{
    xhr.setRequestHeader(csrftokenname, window[csrftokenname]);
}

function displayList(uri, hanlebarTmplId, destElementId)
{
    $.ajax({
        url: uri,
        type: 'GET',
        dataType: 'json',
        beforeSend: genericBeforeSend,
        success: function(resp) {
            var tmpl = Handlebars.compile($('#'+hanlebarTmplId).html());
            var context = {list: resp.data};
            $('#'+destElementId).html(tmpl(context));
            applyChzn();
        }
    });
}

function alertBox(type, message)
{
    var tmpl = Handlebars.compile($('#alert-hb-tmpl').html());
    var context = {
        type: type,
        message: message
    };
    return tmpl(context);
}

function displayMessage(resp, id)
{
    var e = $('#'+(id||'status-message'));
    e.html(alertBox(resp.error?'danger':'success', resp.message));
    e.find('.alert').alert();
}

function applyChzn()
{
    var chzn = $(".chzn-select");
    if (chzn.length) {
        $(".chzn-select").chosen();
    }
}

$(document).ready(function(){
    var logoutEle = $('a[href="/api/authentication/logout"]');
    if (logoutEle.length) {
        logoutEle.on('click', function(e){
            e.preventDefault();
            $.ajax({
                url: logoutEle.attr('href'),
                beforeSend: genericBeforeSend,
                success: function() {
                    window.location.reload();
                }
            });
            return false;
        });
    }
});