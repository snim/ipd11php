function getCourseList()
{
    displayList('/api/course/getlist', 'course-list-hb-tmpl', 'course-select-opts');
}

function getStudentList()
{
    displayList('/api/student/getlist', 'student-list-hb-tmpl', 'student-select-opts');
}

$(document).ready(function () {
    var formId = 'course-reg-form';
    formHandler($('#'+formId), null, null, genericBeforeSend);
    getCourseList();
    getStudentList();
});