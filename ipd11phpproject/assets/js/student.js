function getStudentList(resp)
{
    if (resp) {
        displayMessage(resp);
    }
    displayList('/api/student/getlist', 'student-hb-tmpl', 'student-list');
}

$(document).ready(function () {
    var formId = 'student-form';
    formHandler($('#'+formId), getStudentList, null, genericBeforeSend);
    getStudentList();
});