function studentByCourseList(resp)
{
    var tmpl = Handlebars.compile($('#list-hb-tmpl').html());
    console.log({data: resp.data});
    $('#studentbycourse-list').html(tmpl({data: resp.data}));
}

function getCourseList()
{
    displayList('/api/course/getlist', 'course-list-hb-tmpl', 'course-select-opts');
}

$(document).ready(function () {
    var formId = 'studentbycourse-form';
    formHandler($('#'+formId), studentByCourseList, null, genericBeforeSend);
    getCourseList();
});