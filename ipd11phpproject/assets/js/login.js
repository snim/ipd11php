function onLoginSuccess(resp, form)
{
    var statusMessageContainer = $('#status-message');
    statusMessageContainer.html(alertBox(resp.error?'danger':'success', resp.message));
    if (resp.success) {
        setTimeout(function () {
            window.location.reload();
        }, 1000);
    }
    statusMessageContainer.find('.alert').alert();
}

function onLoginError(xhr, textStatus, throwErr, form)
{
    $('#status-message').html(alertBox('danger', 'Error occured: '+textStatus));
}

function onLoginBeforeSend(xhr, form)
{
    genericBeforeSend(xhr);
    var username = form.find('#username').val();
    var password = form.find('#password').val();
    xhr.setRequestHeader('Authorization', btoa(username + ":" + password));
}

$(document).ready(function () {
    var formElement = $('#login-form');
    formHandler(formElement, onLoginSuccess, onLoginError, onLoginBeforeSend, true);
});