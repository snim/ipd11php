<?php include __DIR__ . '/defines.php'; ?>
<html>
    <head>
        <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/assets/css/style.css" />
        <link rel="stylesheet" href="/assets/css/chosen.css" />
        <title>IPD11 - PHP - Project</title>
    </head>
    <body>
        <div class="container">
            <?php if (\Ezpz\Common\Authentication::isAuthed()) {
                include INCLUDES_DIR . '/header.php'; ?>
                <main role="main">
                    <?php include \Ezpz\Common\Request::contentFile(); ?>
                </main>
                <?php include INCLUDES_DIR . '/footer.php';
            }
            else {
                include INCLUDES_DIR . '/content/login.php';
            }
            ?>
        </div>
        <script id="alert-hb-tmpl" type="text/x-handlebars-template">
            <div class="alert alert-{{type}} alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <div class="message">{{message}}</div>
            </div>
        </script>
        <?php echo '<script> var csrftokenname= "'.CSRF_TOKEN_NAME.'", '.CSRF_TOKEN_NAME.' = "'. getCSRFToken().'";</script>';?>
        <script src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.11/handlebars.min.js"></script>
        <script src="/assets/bootstrap/js/jquery-v3.2.1.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
        <script src="/assets/bootstrap/js/popper.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap-v4.0.0-beta.3.min.js"></script>
        <script src="/assets/js/chosen.min.js"></script>
        <script src="/assets/js/script.js"></script>
        <?php
        foreach(\Ezpz\Common\Page::getScripts() as $jsfile) {
            ?><script src="<?php echo $jsfile;?>"></script><?php
        }
        ?>
    </body>
</html>