<?php

namespace ProjectApp\RequestHandler;

use \Ezpz\RequestHandler\DataSanitizer;

class POSTHandler extends \ProjectApp\RESTHandler {

    public function process() {
        
        foreach($_POST as $k=>$v) {
            if ($k !== 'uri') {
                $this->data[$k] = DataSanitizer::sanitize($k, 'post');
            }
        }
    }
}
