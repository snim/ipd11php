<?php

namespace ProjectApp\RequestHandler;

use \Ezpz\RequestHandler\DataSanitizer;

class GETHandler extends \ProjectApp\RESTHandler {

    public function process() {
        
        foreach($_GET as $k=>$v) {
            if ($k !== 'uri') {
                $this->data[$k] = DataSanitizer::sanitize($k, 'get');
            }
        }
    }
}
