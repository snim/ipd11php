<?php

namespace ProjectApp;

use Ezpz\Database\DbSettings;
use Ezpz\Database\Dbo;

abstract class ContextProcessorServiceAbstract
{
    protected $output = array();
    protected $uriParts = array();
    private static $dbo = null;
    
    abstract public function execute();
    
    final public function setUriParts(array $uriParts)
    {
        $this->uriParts = $uriParts;
    }
    
    final public function requestHandler() {
        $method = strtoupper($_SERVER['REQUEST_METHOD']);
        $request = 'ProjectApp\\RequestHandler\\'.$method.'Handler';
        $requestHandler = new $request;
        if ($requestHandler instanceof RESTHandler) {
            $requestHandler->process();
        }
        return $requestHandler;
    }
    
    final protected function getDbo()
    {
        if (self::$dbo === null) {
            $dbSettings = new DbSettings('mysql', 'localhost', 'test', 'test', 'ipd11phpproject');
            self::$dbo = new Dbo($dbSettings);
        }
        return self::$dbo;
    }
    
    public final function getOutputAsArray()
    {
        return $this->output;
    }
    
    final public function getHeader($param)
    {
        $headers = getallheaders();
        if (isset($headers[$param]))
        {
            return $headers[$param];
        }
        
        return null;
    }
}