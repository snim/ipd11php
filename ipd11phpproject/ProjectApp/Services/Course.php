<?php

namespace ProjectApp\Services;

class Course extends \ProjectApp\ContextProcessorServiceAbstract
{
    private $table = 'courses';
    
    public function execute()
    {
        // check if 
        if (sizeof($this->uriParts) && $this->uriParts[0])
        {
            if (method_exists($this, $this->uriParts[0]))
            {
                $this->{$this->uriParts[0]}();
            }
            else
            {
                $this->output = array(
                    'error' => true,
                    'message' => 'Method '. $this->uriParts[0] . ' does not exist!'
                    );
            }
        }
        else
        {
            $this->output = array(
                    'error' => true,
                    'message' => 'Illegal request.'
                    );
        }
    }
    
    private function getlist()
    {
        $dbo = $this->getDbo();
        $statement = 'SELECT c.*'
                . ',(SELECT COUNT(sc.student_id) FROM student_courses sc WHERE sc.course_id=c.id) AS numStudents '
                . 'FROM ' . $this->table . ' AS c';
        $this->output = array(
            'data' => $dbo->loadAssocList($statement),
            'success' => true,
            'message' => 'Success!'
        );
    }
    
    public function add()
    {
        $dbo = $this->getDbo();
        $values = array();
        $data = $this->requestHandler()->getDataAsArray();
        foreach($data as $key=>$value) {
            $values[] = $dbo->quote($value);
        }
        $statement = 'INSERT INTO '.$this->table.'(code,name,description) VALUES('.implode(',',$values).')';
        $dbo->query($statement);
        $this->output = array(
            'success' => true,
            'message' => 'Success!'
        );
    }
    
    public function addstudent()
    {
        $dbo = $this->getDbo();
        $values = array();
        $data = $this->requestHandler()->getDataAsArray();
        $values[] = $dbo->quote($data['studentid']);
        $values[] = $dbo->quote($data['courseid']);
        $statement = 'INSERT INTO student_courses(student_id,course_id) VALUES('.implode(',',$values).')';
        $dbo->query($statement);
        $this->output = array(
            'success' => true,
            'message' => 'Success!'
        );
    }
}
