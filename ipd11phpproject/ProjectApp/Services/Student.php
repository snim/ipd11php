<?php

namespace ProjectApp\Services;

class Student extends \ProjectApp\ContextProcessorServiceAbstract
{
    private $table = 'students';
    
    public function execute()
    {
        // check if 
        if (sizeof($this->uriParts) && $this->uriParts[0])
        {
            if (method_exists($this, $this->uriParts[0]))
            {
                $this->{$this->uriParts[0]}();
            }
            else
            {
                $this->output = array(
                    'error' => true,
                    'message' => 'Method '. $this->uriParts[0] . ' does not exist!'
                    );
            }
        }
        else
        {
            $this->output = array(
                    'error' => true,
                    'message' => 'Illegal request.'
                    );
        }
    }
    
    private function getlist()
    {
        $dbo = $this->getDbo();
        $statement = 'SELECT * FROM ' . $this->table;
        $this->output = array(
            'data' => $dbo->loadAssocList($statement),
            'success' => true,
            'message' => 'Success!'
        );
    }
    
    private function add()
    {
        $dbo = $this->getDbo();
        $values = array();
        $data = $this->requestHandler()->getDataAsArray();
        foreach($data as $key=>$value) {
            $values[] = $dbo->quote($value);
        }
        $statement = 'INSERT INTO '.$this->table.'(first_name,last_name,dob) VALUES('.implode(',',$values).')';
        $dbo->query($statement);
        $this->output = array(
            'success' => true,
            'message' => 'Success!'
        );
    }
    
    private function by()
    {
        $last = end($this->uriParts);
        $error = true;
        if ($last === 'course')
        {
            $data = $this->requestHandler()->getDataAsArray();
            $code = isset($data['code']) ? $data['code'] : null;
            if ($code)
            {
                $error = false;
                $dbo = $this->getDbo();
                $statement = 'SELECT s.* '
                        . 'FROM student_courses sc '
                        . 'JOIN courses c ON c.id=sc.course_id '
                        . 'LEFT JOIN ' . $this->table .' AS s ON s.id=sc.student_id '
                        . 'WHERE c.code='.$dbo->quote($code);
                $results = $dbo->loadAssocList($statement);
                $data = array();
                if ($results) {
                    $data['code'] = $code;
                    $data['numStudents'] = sizeof($results);
                    $data['numRows'] = sizeof($results)+1;
                    $data['list'] = $results;
                }
                $this->output = array(
                    'data' => $data,
                    'success' => true,
                    'message' => 'Success!'
                );
            }
        }
        
        if ($error)
        {
            $this->output = array(
                'error' => true,
                'message' => 'Illegal request!'
            );
        }
    }
}
