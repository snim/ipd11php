<?php

namespace Ezpz\Common;

final class Page
{
    private static $scripts = array();
    
    public static function navItems()
    {
        $navItems = array(
            array(
                'url' => '/students',
                'text' => 'Students'
            ),
            array(
                'url' => '/courses',
                'text' => 'Courses'
            ),
            array(
                'url' => '/course-registration',
                'text' => 'Register Course'
            ),
            array(
                'url' => '/courses-per-student',
                'text' => 'Show courses by student'
            ),
            array(
                'url' => '/api/authentication/logout',
                'text' => 'Logout'
            )
        );
        
        return $navItems;
    }
    
    public static function addScript($jsfile)
    {
        if (!in_array($jsfile, self::$scripts))
        {
            self::$scripts[] = $jsfile;
        }
    }
    
    public static function getScripts()
    {
        return self::$scripts;
    }
}