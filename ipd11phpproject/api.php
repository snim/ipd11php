<?php

// set response header to be JSON type
// with character set UTF-8
header('Content-Type: application/json; charset=UTF-8');

include __DIR__. '/defines.php';

// checking if this request was sent via ajax
$xhr = isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? 
        strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) :  null;
$isAjaxRequest = $xhr === 'xmlhttprequest';

if ($isAjaxRequest)
{
    // param uri passed by .htaccess
    $uri = isset($_GET['uri']) ? $_GET['uri'] : '';

    // if uri exists
    if ($uri)
    {
        if (checkCsrfToken() && 
            ($uri === 'authentication/login' || \Ezpz\Common\Authentication::isAuthed()))
        {
            $cp = new ProjectApp\ContextProcessor();
            $cp->process($uri);
            echo json_encode($cp->getOutputAsArray());
        }
        else
        {
            echo json_encode(array(
                'error'=>true,
                'message'=> 'Illegal request'
                ));
        }
    }
    else
    {
        echo json_encode(array(
            'error'=>true,
            'message'=> 'Illegal request'
            ));
    }
}
else
{
    if (!\Ezpz\Common\Authentication::isAuthed()) {
        echo json_encode(array(
            'error'=>true,
            'message'=> 'Log in first.'
            ));
    }
    else {
        echo json_encode(array(
            'error'=>true,
            'message'=> 'Illegal request'
            ));
    }
}