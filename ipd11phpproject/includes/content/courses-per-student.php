<h1>Students by course</h1>
<form action="/api/student/by/course" method="post" class="form-inline" id="studentbycourse-form">
    <div class="row">
        <div class="form-group mx-sm-3 mb-2">
            <label for="code" class="sr-only">Course code</label>
            <div id="course-select-opts"></div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary mb-2">Go</button>
</form>
<div id="studentbycourse-list"></div>
<script id="list-hb-tmpl" type="text/x-handlebars-template">
    <table class="table table-bordered">
        <thead align='center'>
            <tr>
                <th>Code & # of students</th>
                <th>First name</th>
                <th>Last name</th>
                <th>Date of birth</th>
            </tr>
        </thead>
        <tbody>
            {{#if data}}
            <tr>
                <td rowspan="{{data.numRows}}">
                    Course code: {{data.code}} & # of student: {{data.numStudents}}
                </td>
            </tr>
            {{#each data.list}}
            <tr>
                <td>{{first_name}}</td>
                <td>{{last_name}}</td>
                <td>{{dob}}</td>
            </tr>
            {{/each}}
            {{else}}
            <tr><td colspan="4">No data.</td></tr>
            {{/if}}
        </tbody>
    </table>
</script>
<script id="course-list-hb-tmpl" type="text/x-handlebars-template">
    <select class="form-control" name="code" data-paceholder="Select course" id="code" aria-describedby="codeHelp"
        data-validation="required">
    {{#if list}}
        <option value="">Select course</option>
    {{#each list}}
        <option value="{{code}}">{{name}} ({{code}})</option>
    {{/each}}
    {{else}}
    <option value="">No course</option>
    {{/if}}
    </select>
</script>
<?php
\Ezpz\Common\Page::addScript('/assets/js/course-per-student.js');