<h1>List of Students</h1>
<table class="table table-bordered">
    <thead align='center'>
        <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>Date of birth</th>
        </tr>
    </thead>
    <tbody id='student-list'></tbody>
</table>
<script id="student-hb-tmpl" type="text/x-handlebars-template">
    {{#if list}}
    {{#each list}}
    <tr>
        <td>{{first_name}}</td>
        <td>{{last_name}}</td>
        <td>{{dob}}</td>
    </tr>
    {{/each}}
    {{else}}
    <tr><td colspan="3">No data.</td></tr>
    {{/if}}
</script>
<form action="/api/student/add" method="post" id="student-form">
    <h1>Add Student</h1>
    <div id="status-message"></div>
    <div class="form-group">
        <label for="first_name">First name</label>
        <input type="text" class="form-control" name="first_name" id="first_name" aria-describedby="first_nameHelp" placeholder="First name" 
               data-validation="required">
        <small id="first_nameHelp" class="form-text text-muted">First name.</small>
    </div>
    <div class="form-group">
        <label for="last_name">Last name</label>
        <input type="text" class="form-control" name="last_name" id="last_name" aria-describedby="last_nameHelp" placeholder="Last name"
               data-validation="required">
        <small id="last_nameHelp" class="form-text text-muted">Last name.</small>
    </div>
    <div class="form-group">
        <label for="dob">DoB</label>
        <input type="Date" class="form-control" name="dob" id="dob" aria-describedby="dobHelp" placeholder="Date of birth" 
               data-validation="required">
        <small id="dobHelp" class="form-text text-muted">Date of birth.</small>
    </div>
    <button type="submit" class="btn btn-primary">Add</button>
</form>

<?php
\Ezpz\Common\Page::addScript('/assets/js/student.js');