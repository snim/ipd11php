<h1>Course Registration</h1>
<form action="/api/course/addstudent" method="post" id="course-reg-form">
    <div class="form-group">
        <div id="status-message"></div>
        <label for="studentid">Student</label>
        <div id="student-select-opts"></div>
        <small id="studentidHelp" class="form-text text-muted">List of students.</small>
    </div>
    <div class="form-group">
        <label for="courseid">Course</label>
        <div id="course-select-opts"></div>
        <small id="courseidHelp" class="form-text text-muted">List of courses.</small>
    </div>
    <button type="submit" class="btn btn-primary">Register</button>
</form>

<script id="student-list-hb-tmpl" type="text/x-handlebars-template">
    <select class="form-control" name="studentid" id="studentid" aria-describedby="studentidHelp"
        data-validation="required">
    {{#if list}}
        <option value="">Select student</option>
    {{#each list}}
        <option value="{{id}}">{{first_name}} {{last_name}}</option>
    {{/each}}
    {{else}}
    <option value="">No student</option>
    {{/if}}
    </select>
</script>
<script id="course-list-hb-tmpl" type="text/x-handlebars-template">
    <select class="form-control" name="courseid" id="courseid" aria-describedby="courseidHelp"
        data-validation="required">
    {{#if list}}
        <option value="">Select course</option>
    {{#each list}}
        <option value="{{id}}">{{name}} ({{code}})</option>
    {{/each}}
    {{else}}
    <option value="">No course</option>
    {{/if}}
    </select>
</script>
<?php
\Ezpz\Common\Page::addScript('/assets/js/course-registration.js');