<h1>List of Courses</h1>
<table class="table table-bordered">
    <thead align='center'>
        <tr>
            <th>Code</th>
            <th>Name</th>
            <th>Description</th>
            <th># of students</th>
        </tr>
    </thead>
    <tbody id='course-list'></tbody>
</table>
<script id="course-hb-tmpl" type="text/x-handlebars-template">
    {{#if list}}
    {{#each list}}
    <tr>
        <td>{{code}}</td>
        <td>{{name}}</td>
        <td>{{description}}</td>
        <td>{{numStudents}}</td>
    </tr>
    {{/each}}
    {{else}}
    <tr><td colspan="4">No data.</td></tr>
    {{/if}}
</script>
<form action="/api/course/add" method="post" id="course-form">
    <h1>Add Course</h1>
    <div id="status-message"></div>
    <div class="form-group">
        <label for="code">Code</label>
        <input type="text" class="form-control" name="code" id="code" aria-describedby="codeHelp" placeholder="Course code" 
               data-validation="required">
        <small id="codeHelp" class="form-text text-muted">Course code.</small>
    </div>
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" id="name" aria-describedby="nameHelp" placeholder="Course name" 
               data-validation="required">
        <small id="nameHelp" class="form-text text-muted">Course name.</small>
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" name="description" id="description" aria-describedby="descriptionHelp" placeholder="Course description"
                  data-validation="required"></textarea>
        <small id="descriptionHelp" class="form-text text-muted">Course description.</small>
    </div>
    <button type="submit" class="btn btn-primary">Add</button>
</form>

<?php
\Ezpz\Common\Page::addScript('/assets/js/course.js');