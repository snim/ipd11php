<?php

define('ROOT', __DIR__);
define('INCLUDES_DIR', ROOT.'/includes');
define('CONTENT_DIR', INCLUDES_DIR.'/content');
define('PROJECT_APP_DIR', ROOT.'/ProjectApp');
define('VENDOR_DIR', ROOT.'/vendor');

define('SECRET', 'aAq0p10dDaA');
define('SESSION_ID', 'sid'.md5(SECRET));
define('CSRF_TOKEN_NAME', 'csrftoken');


include VENDOR_DIR . '/autoload.php';
include ROOT . '/autoload.php';
include ROOT . '/sessionInit.php';

Ezpz\CustomErrorHandler::init();