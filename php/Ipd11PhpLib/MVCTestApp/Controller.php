<?php

namespace MVCTestApp;

class Controller extends BaseController
{
    private $model;

    public function __construct(Model $model){
        $this->model = $model;
    }

    public function load() {
        
    }
    
    public function insert() {
        
    }
    
    public function update() {
        
    }
    
    public function delete() {
        
    }
}